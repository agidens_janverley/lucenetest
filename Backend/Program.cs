﻿using System;
using System.Collections.Generic;
using System.IO;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Core;
using Lucene.Net.Analysis.NGram;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Highlight;
using Lucene.Net.Store;
using Lucene.Net.Util;
using Directory = Lucene.Net.Store.Directory;

namespace Backend
{
    internal class MyAnalyser : Analyzer
    {
        private readonly LuceneVersion luceneVersion;

        public MyAnalyser(LuceneVersion luceneVersion)
        {
            this.luceneVersion = luceneVersion;
        }

        protected override TokenStreamComponents CreateComponents(string fieldName, TextReader reader)
        {
            var src = new NGramTokenizer(luceneVersion, reader,2,5);
            TokenStream tok = 
                new LowerCaseFilter(
                    luceneVersion,
                    new StandardFilter(luceneVersion, src));
            return new TokenStreamComponents(src, tok);
        }
    }

    internal class Program
    {
        private static void AddDoc(IndexWriter indexWriter, string name, string city, string country)
        {
            var doc = new Document();

            doc.Add(new TextField("name", name, Field.Store.YES));
            doc.Add(new TextField("city", city, Field.Store.YES));
            doc.Add(new TextField("country", country, Field.Store.YES));

            indexWriter.AddDocument(doc);
        }


        public static void Main(string[] args)
        {
            using (var analyser = new MyAnalyser(LuceneVersion.LUCENE_48))
            {
                var index = new RAMDirectory();
                using (var indexWriter = new IndexWriter(index, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyser)))
                {
                    AddDoc(indexWriter, "De Backer", "Antwerpen", "Belgium");
                    AddDoc(indexWriter, "De Backer NV", "Mechelen", "Belgium");
                    AddDoc(indexWriter, "De Backer BVBA", "Deurne", "Belgium");
                }


                //ManualEntry(analyser, index);

                var testInputs = new List<string> {"de", "DE", "de backer", "de nv", "de bv", "debv"};

                //TestFuzzy(analyser, index, testInputs);
                TestBooleanQuery(analyser, index, testInputs);
            }
        }

        private static void TestBooleanQuery(Analyzer analyser, Directory index, IEnumerable<string> testInputs)
        {
            Console.WriteLine("Boolean query");

            foreach (var testInput in testInputs)
            {
                    var queryBuilder = new QueryBuilder(analyser); 
                    var q = queryBuilder.CreateBooleanQuery("name", testInput, Occur.SHOULD);
                //Console.WriteLine($"Query: {q}");

                using (var indexReader = DirectoryReader.Open(index))
                {
                    var indexSearcher = new IndexSearcher(indexReader);

                    var formatter = new Highlighter(new SimpleHTMLFormatter("**", "**"),
                        new DefaultEncoder(), new QueryScorer(q, null));

                    var docs = indexSearcher.Search(q, 10);

                    Console.WriteLine($"Input: [\"{testInput}\"] - Found {docs.ScoreDocs.Length} results:");

                    foreach (var scoreDoc in docs.ScoreDocs)
                    {
                        var docId = scoreDoc.Doc;
                        var document = indexSearcher.Doc(docId);
                        //Console.WriteLine(document.ToString());

                        var x = formatter.GetBestFragments(analyser, "name", document.Get("name"), 4);
                        foreach (var y in x)
                        {
                            Console.WriteLine(y);
                        }

                        //Console.WriteLine($"Result: {document.Get("name")}");
                    }
                }
            }
           
        }

        private static void TestFuzzy(Analyzer analyser, Directory index, IEnumerable<string> testInputs)
        {
            Console.WriteLine("Fuzzy query");

            foreach (var testInput in testInputs)
            {
                var q = new FuzzyQuery(new Term("name", testInput), 2, 5, 10, false);
                Console.WriteLine($"Query: {q}");

                using (var indexReader = DirectoryReader.Open(index))
                {
                    var indexSearcher = new IndexSearcher(indexReader);

                    var formatter = new Highlighter(new SimpleHTMLFormatter("**", "**"),
                        new DefaultEncoder(), new QueryScorer(q, null));

                    var docs = indexSearcher.Search(q, 10);

                    Console.WriteLine($"Input: [\"{testInput}\"] - Found {docs.ScoreDocs.Length} results:");

                    foreach (var scoreDoc in docs.ScoreDocs)
                    {
                        var docId = scoreDoc.Doc;
                        var document = indexSearcher.Doc(docId);

                        var x = formatter.GetBestFragments(analyser, "name", document.Get("name"), 4);
                        foreach (var y in x)
                        {
                            Console.WriteLine(y);
                        }

                        //Console.WriteLine($"Result: {document.Get("name")}");
                    }
                }
            }
        }

        private static void ManualEntry(Analyzer analyser, Directory index)
        {
            while (true)
            {
                var input = Console.ReadLine();
                if (String.IsNullOrEmpty(input))
                {
                    break;
                }

//                    var queryBuilder = new QueryBuilder(analyser); 
//                    var q = queryBuilder.CreateBooleanQuery("name", input, Occur.SHOULD);

                var q = new FuzzyQuery(new Term("name", input), 2, 5, 10, false);


                using (var indexReader = DirectoryReader.Open(index))
                {
                    var indexSearcher = new IndexSearcher(indexReader);

                    var formatter = new Highlighter(new SimpleHTMLFormatter("**", "**"),
                        new DefaultEncoder(), new QueryScorer(q, null));

                    var docs = indexSearcher.Search(q, 10);

                    Console.WriteLine($"Found {docs.ScoreDocs.Length} results:");

                    foreach (var scoreDoc in docs.ScoreDocs)
                    {
                        var docId = scoreDoc.Doc;
                        var document = indexSearcher.Doc(docId);

                        var x = formatter.GetBestFragments(analyser, "name", document.Get("name"), 4);
                        foreach (var y in x)
                        {
                            Console.WriteLine(y);
                        }

                        //Console.WriteLine($"Result: {document.Get("name")}");
                    }
                }
            }
        }
    }
}